﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    [HideInInspector] public enum GameStatus {STAY,FINISH,RESTART }
    [HideInInspector] public GameStatus _gameStatus;
    [SerializeField] private Transform _canvas;
    void Start()
    {
        _gameStatus = GameStatus.STAY;
        _canvas.Find("LevelDisplay").GetChild(0).GetComponent<Text>().text = SceneManager.GetActiveScene().name;
    }

    // Update is called once per frame
    void Update()
    {
        switch (_gameStatus)
        {
            case GameStatus.STAY:
                break;
            case GameStatus.FINISH:
                _canvas.Find("Finish").gameObject.SetActive(true);
                break;
            case GameStatus.RESTART:
                _canvas.Find("ParentRestart").gameObject.SetActive(true);
                break;
            default:
                break;
        }
    }
    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void FinishButton()
    {
        Debug.Log(SceneManager.sceneCountInBuildSettings +"    "+ SceneManager.GetActiveScene().buildIndex);
        if (SceneManager.sceneCountInBuildSettings == SceneManager.GetActiveScene().buildIndex+1)
        {
            SceneManager.LoadScene(0);
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        }
    }
}
