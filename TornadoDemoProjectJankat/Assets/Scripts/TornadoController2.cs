﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Touch;

public class TornadoController2 : MonoBehaviour
{
    [HideInInspector] public enum TouchControl { EMPTY, DOWN, UP }
    [HideInInspector] public TouchControl touchControl;
    [SerializeField] private LayerMask _layerMask;
    private GameController _gameController;
    private CarController _carController;
    private Transform _carTransform;

    private Vector3 _deltaPosition;
    void Start()
    {
        Init();
        LeanTouchSetUp();

    }
    private void LeanTouchSetUp()
    {
        LeanTouch.OnFingerSet += OnFingerSet;
        LeanTouch.OnFingerUp += OnFingerUp;
        LeanTouch.OnFingerDown += OnFingerDown;
        touchControl = TouchControl.EMPTY;
    }
    private void Init()
    {
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
        _carController = GameObject.FindGameObjectWithTag("Player").GetComponent<CarController>();
        _carTransform = _carController.transform;
    }

    private void OnFingerUp(LeanFinger obj)
    {
        touchControl = TouchControl.UP;
    }
    private void OnFingerDown(LeanFinger obj)
    {
        touchControl = TouchControl.DOWN;

    }
    private void OnFingerSet(LeanFinger obj)
    {
        _deltaPosition = obj.ScreenPosition - obj.LastScreenPosition;
    }

    void FixedUpdate()
    {
        // Bura ekrana temas oldugunda hortumu kaydirmak icin kullanilan kisim
        if (touchControl == TouchControl.DOWN)
        {
            transform.rotation = _carTransform.rotation;

            Vector3 leanScreenDelta = _deltaPosition * 100; //LeanGesture.GetScreenDelta()*100;

            // Hizi sabitliyorum kuvvet surekli uygulanacagi icin ucup gitmesin hortum 
            leanScreenDelta.Set(Mathf.Clamp(leanScreenDelta.x, -3000, 3000), 0, Mathf.Clamp(leanScreenDelta.y, -3000, 3000));
            leanScreenDelta = transform.TransformDirection(leanScreenDelta);
            GetComponent<Rigidbody>().AddForce(leanScreenDelta);
        }
        // Sabit ilerleme hareketi
        transform.Translate(_carTransform.forward * Time.fixedDeltaTime * 10, Space.World);
    }
    private void LateUpdate()
    {
        TornadoYAxis();
    }
    private void TornadoYAxis()
    {
        if (Physics.Raycast(transform.position, Vector3.down, out RaycastHit hit, _layerMask))
        {
            Debug.DrawLine(transform.position, hit.point, Color.red);
            transform.position = Vector3.Lerp(transform.position, new Vector3(transform.position.x, hit.point.y + Vector3.up.y * 2, transform.position.z), 0.1f);
            
            if (hit.collider.tag == "Player")
            {
                Time.timeScale = 0.4f;
                Destroy(_carController);
                _gameController._gameStatus = GameController.GameStatus.RESTART;
            }

        }
        else
        {

        }
    }
    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "CubeTag")
        {
            float distance = Vector3.Distance(other.transform.position, transform.position);
            other.attachedRigidbody.isKinematic = false;

            // cok yakininda degilese objleri cok ittirmez biraz da havaya kaldirir hortumun geldigi bir tik belli olur.
            if (distance > 4f)
            {
                other.attachedRigidbody.AddForce((other.transform.position - transform.position).normalized / 4);
                other.attachedRigidbody.AddForce(0, 0.8f, 0);
            }
            else
            {
                other.attachedRigidbody.AddForce((other.transform.position - transform.position).normalized / distance * 500);
            }

        }
    }
}
