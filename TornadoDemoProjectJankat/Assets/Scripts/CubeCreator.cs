﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCreator : MonoBehaviour
{
    [SerializeField] private PhysicMaterial _physicMat;
    [SerializeField] [Range(1, 10)] private int sizeX, sizeY, sizeZ;
    private Material _cubeMaterial;
    [SerializeField] private Color _color = Color.red;

    /// <summary>
    /// Ufak capta bir kolaylik saglamasi icin yapilan bir editor kodu 
    /// </summary>
    void Start()
    {
        Vector3 pos = transform.position;
        Vector3 firstPos = transform.position;
        _cubeMaterial = new Material(Shader.Find("Standard"));
        _cubeMaterial.SetFloat("_Mode",1);
        _cubeMaterial.SetFloat("_Metallic", 0);
        _cubeMaterial.SetFloat("_Metallic/Smoothness", 0);
       
        // x, y, z si disaridan verilen(editorden) kuplerin olusmasini ve oyun oynanmiyorken nerelerde kuplerin olusacagini gosteren kisim
        for (int t = 0; t < sizeZ; t++)
        {
            for (int i = 0; i < sizeY; i++)
            {
                for (int k = 0; k < sizeX; k++)
                {
                    GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
                    Rigidbody rb = obj.AddComponent<Rigidbody>();
                    rb.mass = 0.1f;
                    rb.drag = 0;
                    rb.angularDrag = 0;
                    BoxCollider boxC = rb.GetComponent<BoxCollider>();
                    boxC.material = _physicMat;
                    obj.transform.position = pos;
                    pos += new Vector3(1f, 0, 0);
                    obj.tag = "CubeTag";
                    obj.layer = 10;
                    rb.isKinematic = true;
                    MeshRenderer msFilter = obj.GetComponent<MeshRenderer>();

                    _cubeMaterial.color = _color;
                    msFilter.material = _cubeMaterial;
                }
                pos.Set(firstPos.x, pos.y, pos.z);
                pos += new Vector3(0, 1f, 0);
            }
            pos.Set(firstPos.x, firstPos.y, pos.z);
            pos += new Vector3(0, 0, 1);
        }

    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        Vector3 pos = transform.position;
        Vector3 firstPos = transform.position;
        for (int t = 0; t < sizeZ; t++)
        {
            for (int i = 0; i < sizeY; i++)
            {
                for (int k = 0; k < sizeX; k++)
                {
                    Gizmos.DrawWireCube(pos, Vector3.one);
                    pos += new Vector3(1f, 0, 0);
                    Gizmos.color = _color;

                }
                pos.Set(firstPos.x, pos.y, pos.z);
                pos += new Vector3(0, 1f, 0);
            }
            pos.Set(firstPos.x, firstPos.y, pos.z);
            pos += new Vector3(0, 0, 1);
        }
    }
#endif
}
