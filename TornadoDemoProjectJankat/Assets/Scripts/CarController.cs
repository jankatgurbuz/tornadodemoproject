﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour
{
    private Transform _pointPosition;
    private Vector3[] _pointVec;
    private int _pointCounter;
    private GameController _gameController;

    void Start()
    {
        Init();
    }
    private void Init()
    {
        Time.timeScale = 1f;

        //Arabanin gidecegi pozisyonlari diziye atiyorum 
        _pointPosition = GameObject.Find("PointPositions").transform;
        _pointVec = new Vector3[_pointPosition.childCount];
        for (int i = 0; i < _pointVec.Length; i++)
        {
            _pointVec[i] = _pointPosition.GetChild(i).position;
            Destroy(_pointPosition.GetChild(i).gameObject);
        }
        //GameController
        _gameController = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameController>();
    }


    void FixedUpdate()
    {
        // Araba Rotasyonu
        Quaternion myQuaternion = Quaternion.LookRotation((_pointVec[_pointCounter] - transform.position).normalized);
        transform.rotation = Quaternion.Lerp(transform.rotation, myQuaternion, 0.1f);

        // Pointler arasinda gidis ve kontrolleri
        transform.position = Vector3.MoveTowards(transform.position, _pointVec[_pointCounter], 0.2f);

        // gidecegi noktaya vardiysa dizimin diger elemanina geciyorum.
        if (transform.position == _pointVec[_pointCounter])
        {
            _pointCounter++;
            if (_pointCounter == _pointVec.Length)
            {
                _gameController._gameStatus = GameController.GameStatus.FINISH;
                Destroy(this);
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "CubeTag")
        {
            Time.timeScale = 0.4f;
            Destroy(this);
            _gameController._gameStatus = GameController.GameStatus.RESTART;
        }
    }
#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            for (int i = 0; i < _pointVec.Length - 1; i++)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(_pointVec[i], 0.5f);
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(_pointVec[i], _pointVec[i + 1]);
            }
            Gizmos.color = Color.black;
            Gizmos.DrawSphere(_pointVec[_pointVec.Length - 1], 0.5f);
        }
        else
        {
            Transform pointNode = GameObject.Find("PointPositions").transform;
            for (int i = 0; i < pointNode.childCount - 1; i++)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawSphere(pointNode.GetChild(i).position, 0.5f);
                Gizmos.color = Color.blue;
                Gizmos.DrawLine(pointNode.GetChild(i).position, pointNode.GetChild(i + 1).position);
            }
            Gizmos.color = Color.black;
            Gizmos.DrawSphere(pointNode.GetChild(pointNode.childCount - 1).position, 0.5f);
        }

    }
#endif
}
